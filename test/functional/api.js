const assert = require('assert');
describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal(-1, [1,2,3].indexOf(4));
    });
  });
});

const chai = require('chai');
const should = chai.should();
const api = require('../../server/routes/api');

describe('API Routes', function() {
  context('Date and Time Combination', function() {
    it('should return a ISO 8601 date and time with valid input', function() {
      const date = '2017/06/10';
      const time = '06:02 AM';

      api.combineDateTime(date, time)
        .should.equal('2017-06-10T06:02:00.000Z');
    });

    it('should return null on a bad date and time', function() {
      const date = '!@#$';
      const time = 'fail';

      should.not.exist(api.combineDateTime(date, time));
    });
  });
});

