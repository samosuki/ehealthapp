# eHealth Ireland Hackathon App Concept - GrassRoots

TOPIC: CONNECTING FOR THE FUTURE: ELDERLY MENTAL HEALTH

## Getting Started

```bash
npm install
ng build
npm start
```
## Development server

The dev server runs on port 1966. Navigate to `http://localhost:1966/`.

## Development

This project uses EditorConfig to standardize text editor configuration.
Visit http://editorconfig.org for details.

This project uses ESLint to detect suspicious code in JavaScript files.
Visit http://eslint.org for details.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

# Angular CLI Development

This project was generated with [Angular CLI](https://github.com/angular/angular-cli).

### Testing

This project uses [Mocha](http://mochajs.org) and [Chai](http://chaijs.com) for testing server-side.

This project uses [Karma](https://karma-runner.github.io) and [Protactor](http://www.protractortest.org/) for testing client-side

To execute tests:

## Running unit tests (client-side)

Run `npm ngtest` to execute the unit tests via Karma

## Running end-to-end tests (client-side)

Run `ng e2e` to execute the end-to-end tests via Protractor.
Before running the tests make sure you are serving the app via `ng serve`.

## Running unit tests (server-side)

Run `npm test` to execute the unit tests via Mocha
