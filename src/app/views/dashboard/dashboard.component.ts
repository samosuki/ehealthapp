import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
let RecordRTC = require('recordrtc/RecordRTC.min');
import { Router } from '@angular/router';
import { ApiService } from '../../shared/api.service'

// import Directive for Modals
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';

declare var require: any;

@Component({
  templateUrl: 'dashboard.component.html'
})

export class DashboardComponent {
  
  private stream: MediaStream;
  private recordRTC: any;
  public myModal;
  public infoModal;

  constructor(private _apiService: ApiService 
             ) { }

  activities: any[];
  specificActivities: any[];

  ngOnInit() {
      this._apiService.getActivities()
        .subscribe(
          data => {
            this.activities = data;
            console.log(this.activities);
          }
        );
  }

  getSpecificActivity(activityid) {
    this._apiService.specificActivities(activityid)
    .subscribe(
      data => {
        this.specificActivities = data;
        console.log(this.specificActivities);
      });
  }


  successCallback(stream: MediaStream) {
    
        var options = {
          mimeType: 'audio/webm;codecs=opus', // audio
          audioBitsPerSecond : 128000
        };
        this.stream = stream;
        this.recordRTC = RecordRTC(stream, options);
        this.recordRTC.startRecording();
      }
    
      errorCallback() {
        //handle error here
      }

      processAudio(audioFile) {
        let recordRTC = this.recordRTC;
        let recordedBlob = recordRTC.getBlob();
        console.log(recordedBlob);
        let file = new File([recordedBlob], 'audio.webm', {
          type: 'audio/webm;codecs=opus'
        });
        var formData = new FormData();
        formData.append('file', file);
        console.log(formData);
        this.saveAudio(formData);
      }
      

      startRecording() {
        let mediaConstraints = {
          audio: true
        };
        navigator.mediaDevices
          .getUserMedia(mediaConstraints)
          .then(this.successCallback.bind(this), this.errorCallback.bind(this));
      }

      saveAudio(formData) {
          console.log(formData);
          this._apiService.saveAudio(formData)
          .subscribe(data => {
            console.log(data);
        });
      }
    
      stopRecording() {

        let recordRTC = this.recordRTC;
        recordRTC.stopRecording(this.processAudio.bind(this));
        let stream = this.stream;
        stream.getAudioTracks().forEach(track => track.stop());
      }
      
    
      download() {
        this.recordRTC.save('audio.webm');
      }
}
