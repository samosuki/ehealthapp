import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _router: Router,
              private _route: ActivatedRoute) { }

  ngOnInit() {
  }

  onClick() {
    this._router.navigate(['/dashboard']);
  }

  onRegister() {
    this._router.navigate(['/register']);
  }

}
