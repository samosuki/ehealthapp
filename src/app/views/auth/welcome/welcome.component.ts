import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(private _router: Router,
    private _route: ActivatedRoute) { }

  ngOnInit() {
  }

  onClickYes() {
    this._router.navigate(['/register']);
  }

}
