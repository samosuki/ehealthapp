import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private _router: Router,
    private _route: ActivatedRoute) { }

  ngOnInit() {
  }

  onSignUp() {
    this._router.navigate(['/quiz']);
  }

}
