import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  constructor(private _router: Router,
    private _route: ActivatedRoute) { }

  ngOnInit() {
  }

  gotoResults() {
    this._router.navigate(['/results']);
  }

}
