import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SpeechComponent } from './speech/speech.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { QuizComponent } from './quiz/quiz.component';
import { ResultsComponent } from './results/results.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: 'speech',
    component: SpeechComponent,
    data: {
      title: 'Speecn Page'
    }
  },
  {
    path: 'welcome',
    component: WelcomeComponent,
    data: {
      title: 'Welcome Page'
    }
  },
  {
    path: 'quiz',
    component: QuizComponent,
    data: {
      title: 'Quiz Page'
    }
  },
  {
    path: 'results',
    component: ResultsComponent,
    data: {
      title: 'Results Page'
    }
  },
  {
    path: 'main',
    component: MainComponent,
    data: {
      title: 'Main Page'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}
