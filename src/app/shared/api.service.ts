import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { environment } from '../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class ApiService {

  rootUrl = environment.envURL;

  constructor(private _http: Http,
              private _router: Router,
              private _route: ActivatedRoute) { }

  saveAudio(audiofile: any): Observable<any> {
    // const body = JSON.stringify(audiofile);
    console.log('saveAudio called');
    console.log(audiofile);
    const body = audiofile;
    const url = this.rootUrl + '/api/upload';
    return this._http.post(url, body)
      .map(response => response.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getActivities() {
    console.log("getActivities called");
    const url = this.rootUrl + '/api/activities/';
    return this._http.get(url)
    .map((res:Response) => {
      const data = res.json().obj;
      console.log(data);
      return data;
    })
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  specificActivities(activityid) {
    console.log("specificActivities called");
    console.log(activityid);
    const url = this.rootUrl + '/api/activities/';
    return this._http.get(url + activityid)
    .map((res:Response) => {
      const data = res.json().obj;
      console.log(data);
      return data;
    })
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}
