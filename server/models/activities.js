const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    fname: {type: String},
    lname: {type: String},
    email: {type: String},
    password: {type: String},
    address: {type: String},
    geolocation: {type: String},
    mobile: {type: String},
    landline: {type: String},
    role: {type: String},
});

module.exports = mongoose.model('Activities', schema);

