const express = require('express');
const router = express.Router();
const moment = require("moment");

// Include Multer
const multer  = require('multer');
const upload = multer({ dest: './uploads/' });
const uploadedFile = upload.single('file');

const Activities = require('../models/activities');

// Imports the Google Cloud client library
const Storage = require('@google-cloud/storage');

// The name of the bucket to access, e.g. "my-bucket"
const bucketName = "newagent-c44d2.appspot.com";

// The name of the local file to upload, e.g. "./local/path/to/file.txt"
const filename = uploadedFile;//"./local/path/to/file.txt";

// Instantiates a client
const storage = Storage();

/**
   * Combine a date and time into an ISO 8601 standard date and time.
   *
   * @example
   * // returns '2017-06-10T06:02:00.000Z'
   * Reservation.combineDateTime('2017/06/10', '06:02 AM')
   *
   * @param {string} date YYYY/MM/DD format
   * @param {string} time H:mm A format
   * @return {string} ISO 8601 standard date and time.
   */
  function combineDateTime(date, time) {
    return moment.utc(`${date} ${time}`, 'YYYY/MM/DD hh:mm A').toISOString();
  }

/* GET api listing. */
router.get('/', function(req, res) {
  res.status(200).json({
    message: 'Success',
    obj: 'api works'
  });
});


router.get('/activities', function(req, res) {
  Activities.aggregate([
    {
        $lookup: {
           from: "persons",
           localField: "assignedto",
           foreignField: "_id",
           as: "Assigned"
        }
    },
    {
        $unwind: "$Assigned"
    },
    {
        $lookup: {
            from: "persons",
            localField: "ownedby",
            foreignField: "_id",
            as: "Owner"
        }
    },
    {
        $unwind: "$Owner"
    }
])
    .exec(function(err, doc) {
      if(err) {
        console.log(err);
        return res.status(404).json({
          title: 'An error occurred',
          error: err
        });
      }
      console.log(doc);
      res.status(200).json({
        message: 'Success',
        obj: doc
      });
    });
});

router.get('/activities/:activityid', function(req, res) {
  //Activities.find({})
  const activityID = req.params.activityid;
  console.log(activityID);

  Activities.aggregate([
    { "$match": { "_id": activityID } },
    {
        $lookup: {
           from: "persons",
           localField: "assignedto",
           foreignField: "_id",
           as: "Assigned"
        }
    },
    {
        $unwind: "$Assigned"
    },
    {
        $lookup: {
            from: "persons",
            localField: "ownedby",
            foreignField: "_id",
            as: "Owner"
        }
    },
    {
        $unwind: "$Owner"
    }
])
    .exec(function(err, doc) {
      if(err) {
        console.log(err);
        return res.status(404).json({
          title: 'An error occurred',
          error: err
        });
      }
      console.log(doc);
      res.status(200).json({
        message: 'Success',
        obj: doc
      });
    });
});

// POST: Upload Audio to Server & Cloud
/*
    @api {post} /api/upload Upload PAudio to Server & Cloud
    @apiName UploadAudio
    @apiParam {file} uploadedFile file
    @apiSuccess {string} secure_url
*/

router.post('/upload', uploadedFile, function(req, res) {

      console.log(uploadedFile);

      console.log(req.file);

      console.log(req.file.filename);

      if(req.file) {
        // Uploads a local file to the bucket
        storage
        .bucket(bucketName)
        .upload(req.file.path)
        .then(() => {
          //console.log(`${filename} uploaded to ${bucketName}.`);
          res.status(200).json({
            message: `Success: uploaded to ${bucketName}.`
           });
        })
        .catch(err => {
          console.error('ERROR:', err);
          res.status(404).json("error");
        });
     } // end if
    }); // end Upload Photo for Costume

module.exports = router;
